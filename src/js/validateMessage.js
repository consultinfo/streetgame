let form =  document.querySelector('form');

window.validateMessage = function (block, message) {
    if (form.querySelector('.form__validityMessage')) {
        form.querySelector('.form__validityMessage').remove();
    }

    if (message) {
        let blockMessage =  document.createElement('span');
        let coordsBlock = block.getBoundingClientRect();
        block.style.position = 'relative';
        blockMessage.className = 'form__validityMessage';
        blockMessage.innerHTML = message;
        blockMessage.style.left = coordsBlock.left + 'px';
        blockMessage.style.top = coordsBlock.top + coordsBlock.height + 11 + 'px';
        form.appendChild(blockMessage);
    }
};