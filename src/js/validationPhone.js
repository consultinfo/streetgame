let phoneCode =  form.querySelector('.input--phone-code');
let phone =  form.querySelector('.input--phone');
//let substrate = form.querySelector('[class*="__substrate"]');
let inputBlock = form.querySelector('[class*="__input-block"]');
let countryCode = '';
let phoneLength = 10;
let mask = '9999999999[ 9{0,9}]';
let phoneNumberReplace;
let widthPhone;
let phoneNumberOnSubmit;
let clickCurrent = 0;
let urlPhoneCode = './local/templates/site/js/phoneCode.min.json';

Inputmask('9[9][9]', {removeMaskOnSubmit: true, placeholder: ''}).mask(phoneCode);
Inputmask(mask, {removeMaskOnSubmit: true, placeholder: ''}).mask(phone);

let deleteToolTip = function () {
    if (inputBlock.querySelector('.tooltip')) {
        inputBlock.querySelector('.tooltip').remove();
    }
    phone.style.backgroundImage = 'none';
};

let createToolTip = function () {
    deleteToolTip();
    inputBlock.style.position = 'relative';
    phone.style.background = 'rgba(149,151,161,.2) url("local//templates/site/img/exclamation-button.svg") calc(100% - 8px) center no-repeat';
    phone.style.backgroundSize = '15px 15px';

    let tooltip = document.createElement('div');
    tooltip.className = 'tooltip';
    tooltip.innerHTML = 'Превышена длина';
    tooltip.style.right = 'calc(57% - ' + 8.5 * phoneLength + 'px)';
    inputBlock.append(tooltip);
};
/*
document.addEventListener('click', function (evt) {
    clickCurrent ++;

    if ((clickCurrent === 1) && (evt.target === phone)) {
        //console.log('Первый клик по телефону');
        phoneCode.value = '+7';
        phoneLength = 10;
        Inputmask('9999999999[ 9{0,9}]', {removeMaskOnSubmit: true, placeholder: ''}).mask(phone);
    }
});*/

let checkPhoneLength = function () {
    if (String(phoneNumberReplace).length > phoneLength) {
        createToolTip();
        //substrate.style.opacity = '1';
        //substrate.textContent = phoneNumberReplace.substr(phoneLength, phone.value.length);
        //substrate.style.left = 7.5 * (widthPhone.length + 1) + 15 + 'px';
    } else {
        deleteToolTip();
        //substrate.style.opacity = '0';
    }
};

phoneCode.addEventListener('focusout', function () {
    if ((phoneCode.value === '+') || (phoneCode.value === '')) {
       // phoneCode.focus();
        message = 'Введите код страны';
        window.validateMessage(phoneCode, message);

    } else {
        message = '';
        window.validateMessage(phoneCode, message);
    }
});

phoneCode.addEventListener('input', function () {
    //console.log(phoneCode.value);

    if (phoneCode.value === '+' || phoneCode.value === '') {
        phoneCode.value = "+";
        //console.log("test");
        phoneCode.focus();
    }

    for (let i = 0; i < parseJson(urlPhoneCode).length; i++) {
        if (parseJson(urlPhoneCode)[i].code === ('+' + phoneCode.value)) {
            countryCode = parseJson(urlPhoneCode)[i].country;
            console.log('Код страны: ' + countryCode);
            phoneLength = parseJson(urlPhoneCode)[i].length;
            if (parseJson(urlPhoneCode)[i].mask !== undefined) {
                mask = parseJson(urlPhoneCode)[i].mask + '[ 9{0,9}]';
            } else {
                mask = '9{0,9}';
            }
        }
        Inputmask(mask, {removeMaskOnSubmit: true, placeholder: ''}).mask(phone);
        //phone.value = '';
        deleteToolTip();
        //substrate.style.opacity = '0';
    }

    if (phoneCode.value.length === 3) {
        phone.focus();
    }

    checkPhoneLength();
});

phone.addEventListener('input', function () {
    phoneNumberReplace = phone.inputmask.unmaskedvalue();
    //console.log(phoneNumberReplace.length);
    //widthPhone = phone.value.substr(0, phoneLength);
    //phoneNumberOnSubmit = phoneCode.value + phoneNumberReplace; //.substr(0, phoneNumberReplace.indexOf('  '));
    //phoneNumberReplace = phoneNumberReplace.replace('  ', '');

    //console.log(phoneNumberOnSubmit, phoneNumberOnSubmit.length);

    if ((phoneNumberReplace.length === 0) || (phoneNumberReplace.length === phoneLength)) {
        phone.classList.remove('input--invalid');
        message = '';
        window.validateMessage(phone, message);
    }

    message = '';
    phone.classList.remove('input--invalid');
    window.validateMessage(phone, message);

    checkPhoneLength();
});


