let codeInput = form.querySelector('.input--code');
let blockSms = form.querySelector('[class*="__block-SMS"]');
let codeBlock = form.querySelector('div[class*=\'__block--code\']');
let passwordBlock = form.querySelector('div[class*=\'__block--password\']');
let againButton = form.querySelector('a[class*=\'__link--again\']');
let message ='';
let timer;
let urlCodeSms = './local/templates/site/js/data.min.json';
let borderInput = form.querySelector('[class*="__border-input"]');

Inputmask('9999', {removeMaskOnSubmit: true, placeholder: ''}).mask(codeInput);

let validateCode = function (code) {
    if (code.value === '') {
        code.classList.remove('input--invalid');
        message = '';
    } else if (parseJson(urlCodeSms).code === code.value) {
        code.classList.remove('input--invalid');
        passwordBlock.style.display = 'flex';
        codeBlock.style.display = 'none';
        pass1.focus();
        againButton.style.display = 'none';
        message = '';
    } else {
        againButton.style.display = 'block';
        message = 'Неправильный код подтверждения';
    };
    window.validateMessage(code, message);
};

codeInput.addEventListener('input', function () {
    if (codeInput.value.length === 0 || codeInput.value.length === 4) {
        validateCode(codeInput);
    }
    //console.log(codeInput.value);
});

codeInput.addEventListener('focusout', function () {
    codeInput.focus();
});

/*
let validateCode = function (code) {
    code = code.join('');
    if (code === '') {
        blockSms.classList.remove('input--invalid');
        message = '';
    } else if (parseJson(urlCodeSms).code === code) {
        blockSms.classList.remove('input--invalid');
        passwordBlock.style.display = 'flex';
        codeBlock.style.display = 'none';
        pass1.focus();
        againButton.style.display = 'none';
        message = '';
    } else {
        againButton.style.display = 'block';
        message = 'Неправильный код подтверждения';
    };
    window.validateMessage(blockSms, message);
};
let count = 0;

(function () {
    for (let a = 0; a <codeInput.length; a++) {
        codeInput[a].addEventListener('input', function (evt) {
            let nextElement = a + 1;
            let prevTubIndex = a - 1;

            if (codeInput[a].value.length === 1) {
                codeSms.push(codeInput[a].value);
                if (nextElement === codeInput.length) {
                    validateCode(codeSms);
                    codeSms = [];
                } else {
                    codeInput[nextElement].focus();
                }
            } else if (codeInput[a].value.length === 0 && prevTubIndex !== -1) {
                count++;
                if (count === codeInput.length - 1) {
                    message = '';
                    window.validateMessage(blockSms, message);
                    count = 0;
                }
                codeInput[prevTubIndex].focus();
            }
        });
    }
})();
 */