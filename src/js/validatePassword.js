let pass1 = passwordBlock.querySelector('#password1');
let pass2 = passwordBlock.querySelector('#password2');
let submitButton = passwordBlock.querySelector('button[type="submit"]');

let validatePass1 = function () {
    if (!(/^[а-яА-ЯёЁa-zA-Z0-9]+$/.test(pass1.value))) {
        pass1.classList.add('input--invalid');
        message = 'Пароль должен содержать только буквы и цифры';
        submitButton.setAttribute('disabled', 'disabled');
    } else {
        pass1.classList.remove('input--invalid');
        message = '';
        submitButton.removeAttribute('disabled', 'disabled');
    }
    window.validateMessage(pass1, message);
};

let validatePass2 = function () {
    if (pass1.value === pass2.value) {
        pass2.classList.remove('input--invalid');
        message = '';
        submitButton.removeAttribute('disabled', 'disabled');
    } else {
        pass2.classList.add('input--invalid');
        message ='Пароли не совпадают. Повторите';
        submitButton.setAttribute('disabled', 'disabled');
    }
    window.validateMessage(pass2, message);
};

pass1.addEventListener('input', function () {
    clearTimeout(timer);
    timer = setTimeout(validatePass1, 700);
});

pass2.addEventListener('input', function () {
    clearTimeout(timer);
    timer = setTimeout(validatePass2, 700);
});

/*
form.addEventListener('submit', function (evt) {
    evt.preventDefault();
    document.location.href = './lk.html';
});
*/