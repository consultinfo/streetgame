let nextStepButton = form.querySelector('.button--code');
let backStepButton = form.querySelector('[class*="__button--back"]')
let userPhone = form.querySelector('[class*="__user-phone"]');
let blockPhone = form.querySelector('[class*="__block--phone"]');

nextStepButton.addEventListener('click', function (evt) {
    evt.preventDefault();
    phoneNumberOnSubmit = '+' + phoneCode.value + phoneNumberReplace; //.substr(0, phoneNumberReplace.indexOf('  '));
    if ((phoneNumberOnSubmit !== undefined) && (String(phoneNumberReplace).length >= phoneLength) && (phoneCode.value != '')) {
        userPhone.value = phoneNumberOnSubmit;
        blockPhone.style.display = 'none';
        codeBlock.style.display = 'flex';
        codeInput.focus();
        message = '';
        phone.classList.remove('input--invalid');
    } else {
        message = 'Проверьте правильность номера телефона:';
        phone.classList.add('input--invalid');
    }
    window.validateMessage(phone, message);
});

backStepButton.addEventListener('click', function (evt) {
    evt.preventDefault();
    codeInput.value = '';
    blockPhone.style.display = 'flex';
    codeBlock.style.display = 'none';
    passwordBlock.style.display = 'none';
    message = '';
    window.validateMessage(blockSms, message);
});

form.addEventListener('submit', function (evt) {
    evt.preventDefault();
    document.location.href = './lk.html';
});
