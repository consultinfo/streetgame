let parseJson = function  (url) {
    let request = new XMLHttpRequest();

    request.open("GET", url, false);
    request.send(null);
    let my_JSON_object = JSON.parse(request.responseText);
    return my_JSON_object;
};