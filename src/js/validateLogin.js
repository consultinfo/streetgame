let urlLogin = './local/templates/site/js/login.min.json';
let loginButton = form.querySelector('button[type="submit"]');
let passwordInput = form.querySelector('#password');
let body = document.querySelector('body');

passwordInput.addEventListener('input', function () {
    message = '';
    window.validateMessage(passwordInput, message);
});

let giveWrong = function () {
    message = 'Неправильный логин или пароль';
    window.validateMessage(passwordInput, message);
};

let giveServerBad = function () {
    document.location.href = './error-404.html';
};

let giveServerGood = function () {
    document.location.href = './lk.html';
};

let giveServerWrong = function () {
    let messageWrong =  document.createElement('div');
    messageWrong.innerHTML = 'Сервер не может обработать ваш запрос. Попробуйте позже.';
    messageWrong.style.left = '0';
    messageWrong.style.bottom = '0';
    messageWrong.style.color = '#fff';
    messageWrong.style.backgroundColor = 'red';
    messageWrong.style.width = '100%';
    body.appendChild(messageWrong);
};

let onSuccess = function (data) {
    loginButton.addEventListener('click', function (evt) {
        evt.preventDefault();
        phoneNumberOnSubmit = '+' + phoneCode.value + phoneNumberReplace;
        console.log(phoneNumberOnSubmit);
        if ((data.login === phoneNumberOnSubmit) && (data.password === passwordInput.value)) {
            message = '';
            giveServerGood();
        } else {
            giveWrong();
        }
        window.validateMessage(passwordInput, message);
    })
};

let onError = function (message) {
    console.error(message);
};

window.load('./local/templates/site/js/login.min.json', onSuccess, onError);

/*
let buttonAuth = document.querySelector ('.button--auth');
let buttonServerBad = document.querySelector ('.button--404');
let buttonServerGood = document.querySelector ('.button--200');
let buttonWrong = document.querySelector ('.button--500');

buttonAuth.addEventListener('click', giveWrong);
buttonServerBad.addEventListener('click', giveServerBad);
buttonServerGood.addEventListener('click', giveServerGood);
buttonWrong.addEventListener('click', giveServerWrong);
 */